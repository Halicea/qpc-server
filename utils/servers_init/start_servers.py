import subprocess
import os
servers = ["mongod", "redis-server", "nginx"]
stopers = {"nginx":"nginx -s stop"}
processes = {}
try:
	for server in servers:
		print "starting", server
		processes[server]=subprocess.Popen(server, env=os.environ)
		print  server, "started"
except (KeyboardInterrupt, SystemExit):
	print "Exiting"
	print processes.keys()
	for k in processes.keys():
		print "Terminating", k
		if stopers.has_key(k):
			exec(stopers[k])
		else:
			servers[k].kill()
	raise
	


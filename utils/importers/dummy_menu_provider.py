# -*- coding: utf-8 -*-
menu =[
  { 
    "Name":u"Охридска Пастрмка (Горички Начин)",
    "Description":u"Рибата се чисти, се мие, пoтoа се суши во чиста салфета, се посолува и се пече на скара. Откако се испече рибата, во посебно тиганче се става маслoтo, лукoт, кромидот и оревите и сетo се пропржува. Рибата се сервира во овал за риби, се прелива со филoт и се декорира со варено јајце, маслинка, лимoн, зелена салата, ротквици и сл.",
    "ImageUrl": "http://www.360macedonia.com/macedonia/mk/images/kujna/kujna_all.jpg",
    "Price": 360,
    "DateRange":{
      "StartDate":None,
      "EndDate":None
    },
    "DayPeriod":{
      "StartTime":None,
      "EndTime":None
    }
  },
  { 
    "Name":u"Неодолива сицилијанска пица",
    "Description":u"Оваа пре­кра­сна пица ќе ве заведе со комбинацијата од медитерански вкусови кои произлегуваат од маслинките.",
    "ImageUrl": "http://gurman.mk/wp/wp-content/uploads/2012/10/sicilijanskapicass.jpg",
    "Price": 365,
    "DateRange":{
      "StartDate":None,
      "EndDate":None
    },
    "DayPeriod":{
      "StartTime":None,
      "EndTime":None
    }
  },
  { 
    "Name":u"Чорба од Ќофтиња",
    "Description":u"На меленото месо додајте ги оризот, црниот пипер, едно јајце и сoл по вкус. Се измешајте добро, направете мали топчиња, извалкајте ги во брашнo и пропржете ги на масло. Врз ќофтињата дoдајте еден литар врела вoда и посолете ги и ставете ги да се варат. Откако ќе се свари супата зачинете ја со две дoбро изматени јајца, во кои сте додале малку киселина. На крајот дoдајте ситно исечкан магданос.",
    "ImageUrl": "http://www.360macedonia.com/macedonia/mk/images/kujna/kujna_all.jpg",
    "Price": 365,
    "DateRange":{
      "StartDate":None,
      "EndDate":None
    } ,
    "DayPeriod":{
      "StartTime":None,
      "EndTime":None
    }
  },
  { 
    "Name":u"Крем од ванила со портокал и карамел",
    "Description":u"Вку­сната синер­гија помеѓу пор­то­ка­лот, вани­лата и кара­ме­лот доаѓа до израз во овој фан­та­сти­чен десерт. Изгледа пре­кра­сно, а за вку­сот и да не збо­ру­ваме. Тоа ви го оста­ваме вам.",
    "ImageUrl": "http://gurman.mk/wp/wp-content/uploads/2012/10/kremkaramel.jpg",
    "Price": 365,
    "DateRange":{
      "StartDate":None,
      "EndDate":None
    } ,
    "DayPeriod":{
      "StartTime":None,
      "EndTime":None
    }
  },
  { 
    "Name":u"Гурабии",
    "Description":u"Ставете на даска за месење брашно, направете на средината вдлабнатина и ставете го маслото, прашокот за пециво и виното. Замесете го тестото средно меко, за да може да се сука. Расукајте од тестото кора и вадете од тестото парчиња со чаша за вода. Истолчете ги оревите, ставете малку цимет и 2 лажици шеќер и помешајте ги. Од овој фил ставете на средината на секое парче и преклопете ги парчињата на половина, така да филот остане на средината. Печете ги така да бидат бледо печени. Откако се испечат, увалкајте ги во шеќер во прав помешан со ванила.",
    "ImageUrl": "http://www.360macedonia.com/macedonia/mk/images/kujna/slatki_all.jpg",
    "Price": 365,
    "DateRange":{
      "StartDate":None,
      "EndDate":None
    } ,
    "DayPeriod":{
      "StartTime":None,
      "EndTime":None
    }   
  }
]

gallery_urls = [
  {"Name":"Dummy Label", "ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRTEp7IOlPzeJBZAWh5gTeeKv6iRkQBxik8y0DhyH9bAvggTv62IQ"},
  {"Name":"Dummy Label", "ImageUrl":"http://t1.gstatic.com/images?q=tbn:ANd9GcSy_gmHIvCHpXe6IK-7pdYYtVwOK6ucfgxdZy-3FoX7K6GPuvAAig"},
  {"Name":"Dummy Label", "ImageUrl":"http://t1.gstatic.com/images?q=tbn:ANd9GcRDf_NONBmRMLDsvhlufCaDwj5CED2TCMpiZiqCIQFz5QuBQqBf"},
  {"Name":"Dummy Label", "ImageUrl":"http://t0.gstatic.com/images?q=tbn:ANd9GcQTASnpcZv2FeoMCpVctTJnow7fthFAXjwx0iA5g-vjtUCycyyiL-NAMMw"},
  {"Name":"Dummy Label", "ImageUrl":"http://t0.gstatic.com/images?q=tbn:ANd9GcS2r1i0_VGg1vcHBBHHJFtn6c13umjYyYp93eJyEwGLXKieA-QR"},
]  

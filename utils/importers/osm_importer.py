import sys
from imposm.parser import OSMParser
from places_repo import save_place, get_by_osmid
from tags_adder import set_tags
 
white_list={'fast_food':5, 'restaurant':1,'cafe':2,
             'nightclub':3, 'pub':1, 'bar':2, 
             'green_market':6, 'arts_centre':6, 
             'marketplace':6, 'theatre':6 , 'bazar':6 }
# 1 Restaurants
# 2 Bars
# 3 Discoteques
# 4 Coctail Bars
# 5 Fast Food       
# 6 Other
class PlacesParser(object):
  counter =0             
  def save_places(self, places):  
    for osmid, tags, loc in places:
      node = {"osmid":osmid, "tags":tags, "loc":loc}
      if self.is_valid_node(node):
        place  = self.parse_place_from_node(node)
        print "geting by osm"
        p = get_by_osmid(place["OsmId"])
        if p:
          place["_id"]=p["_id"]
        set_tags(place)
        save_place(place)   
        print place
        
  def parse_place_from_node(self, node):
    result = {                   
      "OsmId": node["osmid"],
      "Name":node["tags"]["name"],
      "Location":[node["loc"][1], node["loc"][0]],
      "Category":white_list[node["tags"]["amenity"]]
    }
    return result

  def is_valid_node(self, node):
    return node["tags"].has_key('name') \
       and node["tags"].has_key('amenity') \
       and node["tags"]['amenity'] in white_list.keys()
       
def main(map_path):
  np = PlacesParser()
  p = OSMParser(concurrency=4, nodes_callback=np.save_places)
  p.parse(map_path)
  print "ok"
if __name__ =='__main__':
  if len(sys.argv)>1:
    main(sys.argv[1])
  else:
    main("../maps/map-skopje.osm")
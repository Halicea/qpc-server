import config
import pymongo

def save_place(place):
  conn = pymongo.Connection()
  db = conn[config.db_name]
  db["places"].save(place)
  conn.close() 

def get_all():
  conn = pymongo.Connection()
  db = conn[config.db_name]
  res  = list(db["places"].find())
  conn.close()                    
  return res
  
def get_by_id(id):
  conn = pymongo.Connection()
  db = conn[config.db_name]
  res  = db["places"].findOne({"_id":db.ObjectId(id)})
  conn.close()
  return res

def get_by_osmid(osmid):
  conn = pymongo.Connection()
  db = conn[config.db_name]
  res  = db["places"].find_one({"OsmId":osmid})
  conn.close()
  return res


def __tests__():
  get_by_osmid("blabla")
  get_by_id("some_id")

if __name__=="__main__":
  __tests__()
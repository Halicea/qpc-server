import pymongo
import sys
import ascii2cyrillic as ac  
import dummy_menu_provider as dmp
import config
from places_repo import save_place, get_all

def cyrillic2ascii(string):
  return ac.replaceCyrillicWithAscii(string)  
  
def set_tags(place):
  print 'setting tags'
  cyrilic_tags = place["Name"].lower().split(' ')
  latin_tags = []
  for t in cyrilic_tags:
    lt = cyrillic2ascii(t)
    latin_tags.append(lt)
  latin_tags.extend(cyrilic_tags)
  place["Tags"] = latin_tags

i = 0
def set_fake_data(p):
  print 'setting menu'
  p["Images"]= dmp.gallery_urls
  p["Menu"]=  dmp.menu      
  print 'setting offers'
  if i%2 == 0:
    p["SpecialOffers"] = [dmp.menu[3],dmp.menu[2],dmp.menu[0]  ]
  else:
    p["SpecialOffers"] = []


def get_places_to_set():
  return get_all()
  
def main():
  places = get_places_to_set()
  for p in places:
    set_tags(p)
    set_fake_data(p)
    save_place(p)        
    i+=1

if __name__ =="__main__":
  main()
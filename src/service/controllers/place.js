var config = require("../config"),
    mongo = require("mongojs"),  
    utils = require("../lib/utils"),
    db = mongo.connect(config.dbName, config.dbCollections), 
    async = require("async"),   
    winston = require("winston"); 
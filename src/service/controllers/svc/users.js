var config = require('../../config')
  , utils = require('../../lib/utils.js') 
  , mongojs = require("mongojs")
	, db = mongojs.connect(config.dbName, config.dbCollections)
	, items = db.collection("users")
	, https = require('https')
	, validators = require("../../lib/validators");

exports.info = function(req, res){
  if(req.session.user){         
    items.findOne({_id:db.ObjectId(req.session.user._id)}, function(err, user){ 
      if(!err){
        if(user){
          res.json({status:"ok", data:user});
        }else{
          res.json({status:"error", messages:["The users was not found in the database"]});
        }
      }else{
        res.json({status:"errror", messages:[err.toString()]});
      }
    });
  }else{
    res.json({status:"error", messages:["User is not loged in"]});
  }
};

exports.register = {
  facebook: function(req, res){
    try{ 
      validators.validateUser(req.body, function(errors, user){
        if(!err){
          //check if the user exists in the db already
          items.findOne({email:user.email}, function(error, item){
            if(item){
              res.json({status:"error", messages:["There is already registered user with this email"]});
            }else{
              user.role= 'reader'; 
              user.provider= 'facebook';
              items.save(user, function(err){
                if(!err){
                  req.session.user = user;  
                  res.json({status:"ok" , data:req.session.id.toString()});
                }
              });
            }
          });
        }else{
           res.json({status:"not-valid", messages:err});
        }
      });
    }catch(ex){ 
      //Log the error
      res.send({status:"not-valid", messages:[ex.toString()]});
    }
  },
  twitter: function(req, res){
    
  },
  mediawire:function(req, res){
     try{
       validators.validateUser(req.body, function(err, user){
         if(!err){                                           
           validators.validatePassword(req.body, function(err, password){
             if(!err){       
               user.Password = password;
               
               items.findOne({Email:user.Email}, function(error, item){ 
                   if(item){
                     res.json({status:"not-valid", messages:["There is already registered user with this email"]});
                   }else{
                     user.role ='reader';
                     user.provider = 'mediawire';
                     items.save(user, function(err){
                       if(!err){
                         req.session.user = user;
                         res.json({status:"ok", data:req.session.id.toString()});
                       }
                     });
                   }
               });
             }
           });
         }else{
            res.json({status:"not-valid", messages:err});
         }
       });
     }catch(ex){
       res.send({status:"not-valid", messages:[ex.toString()]});
     }
  }
};   

exports.login = {
  facebook : function(req, res){
    var user = req.body;                         
    var options = {
          host: 'graph.facebook.com',
          port: 443,
          path: '/me?access_token='+user.accessToken,
          method: 'GET'
        };                          

    var fbreq = https.request(options, function(fbres){
       var x = '';  
       //fbres.encoding("utf-8"); 
       fbres.on('data', function(chunk){   
         x+=chunk;
       }); 
       fbres.on('error', function(err){
         console.log(err.toString()); 
         res.json({status:"error", message:err.toString()});
       });
       fbres.on('end', function(err){
         item = JSON.parse(x);  
         if(item.Email){
           items.findOne({email:{$regex:item.Email,$options:"-i"}}, function(err, usr){
             if(usr){
               req.session.user = usr;   
               res.json({status:"ok", data:req.session.id.toString()});
             }else{                  
               res.json({status:"error", messages:["Please Register"]});
             }
           });
         }else{
           res.json({status:"error", messages:["Facebook returned an error"]});
         }
      });
    });
    fbreq.end();
    fbreq.on('error', function(){
      res.json({status:"error", messages:["Cannot send request"]});
    });
  },
  twitter : function(req, res){
    
  },
  mediawire : function(req, res){  
    validators.validatePassword(req.body, function(err, password){
      if(!err){
        items.findOne({$and:[{"Email":req.body.Email},
                             {"Password": password}]},
          function(err, user){
            if(user){
              req.session.user = user;
              res.json({status:"ok", data:req.session.id.toString()});
            }else{
              res.json({status:"not-valid", messages:["Invalid Username or Password"]});
            }
          }
        );
      }else{
        res.json({status:"not-valid", message:err});
      }
    });
  },
  auto_mediawire: function(req, res){       
    try{
      items.findOne({$and:[{PlatformType:parseInt(req.body.PlatformType)}, {Id:req.body.Id}]}, function(err, data){
        if(!err)
        {
          if(data){
            req.session.user = data;
            res.json({status:"ok", data:req.session.id.toString()});
          }else{
            items.save(req.body, function(err, data){
              req.session.user = data;
              res.json({status:"ok", data:req.session.id.toString()});
            });
          }
        }else{
          res.json({status:"err", messages:[err]});
        }
      });
    }catch(ex){
      res.json({status:"err", messages:[err]});
    } 
  }
};
exports.save = function(req, res){
  if(req.session.user){
    if(req.session.user.email == req.body.email || req.session.user.role=='admin'){
      validateUser(req.body, function(err, user){
        if(!err){
          items.findOne({email:{$regex:item.Email,$options:"-i"}}, function(err, dbUser){
            if(!err&&user){
              items.update({email:user.Email}, user, function(err) {  
                if(!err){
                  res.json({status:"error", messages:["Cannot Update"]});
                }else{
                  res.json({status:"ok"});
                }
              });
            }else{
              res.json({status:"error", messages:["Cannot find User"]}); 
            }
          });
        }else{
          res.json({status:"error", messages:err});
        }
      });
    }
  } 
};


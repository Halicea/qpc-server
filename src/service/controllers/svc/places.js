var config = require("../../config"),
    mongo = require("mongojs"),  
    utils = require("../../lib/utils"),
    db = mongo.connect(config.dbName, config.dbCollections), 
    async = require("async"),   
    winston = require("winston");
              
var CrudDb = require("../../lib/crud_service").CrudDb;
var placesCrud = new CrudDb("places");
db.collection("places").ensureIndex({Location:"2d"});
db.collection("places").ensureIndex({Tags:1});

function constructQuery(center, radius, category, search_pattern){          
  var rad_radius = 30*radius/6371;
  var q = {
    Location:{$within:{$center:[center, rad_radius]}} 
  };
  if(category){
    q["Category"]={$in: category};
  }
  if(search_pattern){
    q["Tags"]= new RegExp("^"+search_pattern, "i");
  }
  return q;
}
exports.index = function(req, res){
  var center = req.body.MyLocation;
  var radius = req.body.Radius;                     
  var cat = req.body.Categories;                                             
  var q = constructQuery(center, radius, cat, null);   
  utils.dbResponse(req, res, function(c){db.collection("places").find(q, c);});
};          

exports.search = function(req, res){
  db.collection("places").ensureIndex({Tags:1});
  utils.dbResponse(req, res, function(callback){ 
     var center = req.body.MyLocation;
     var radius = req.body.Radius;   
     var cat = req.body.Categories;                
     var search = req.body.SearchString;
     var q = constructQuery(center, radius, cat, search);
     utils.dbResponse(req, res, function(c){db.collection("places").find(q, c);});
  });
};

exports.view = function(req, res){
  utils.dbResponse(req, res, function(callback){  
     db.collection("places").findOne({_id:db.ObjectId(req.params.id)}, callback);      
  });
};

exports.save = function(req, res){         
  utils.dbResponse(req, res, function(callback){    
     db.collection("places").ensureIndex({"Location":"2d"});
     placesCrud.save(req.body,callback)
  });
};

exports.delete = function(req, res){
  utils.dbResponse(req, res, function(callback){  
     placesCrud.delete(req.body,callback)
  });
};  

exports.vote = function(req, res){
  var user = req.session.user;                                                           
  var grade = req.params.grade;
  var idMap = {"_id":placesCrud.db.ObjectId(req.params.id)};
  async.waterfall([
    function(callback){
      db.places.update(idMap, {$inc:{VotersCount:1, VoteSum:grade}}, callback);
    },                   
    function(res, callback){
      session.user.votes.append({ItemId:req.params.id, Vote:grade});
      db.users.update(user, callback);
    },
    function(results, callback){
      db.places.findOne(idMap, callback);
    }
    ], function(error, place){
      if(err){
        utils.sendResponse({status:"error", error:err});
      }else{
        utils.sendResponse({status:"ok", data:results});
      }
  });
};
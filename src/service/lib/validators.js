var	crypto = require('crypto');
exports.validateUser = function (user, callback){   
  var message = [];
  var result = {};
  if(!user.Email) message.push("Email is required"); 
  else result.Email = user.Email;
  if(!user.Gender) message.push("Gender is required");
  else result.Gender = user.Gender;
  if(!user.FirstName) message.push("Name is required");
  else result.FirstName = user.FirstName;
  if(!user.LastName) message.push("Surname is required");
  else result.LastName = user.LastName;  
  if(!user.DOB) message.push("Date of Birth is required");
  else result.DOB = user.DOB;       
  if(user.PreferedCategories) 
    result.PreferedCategories = user.PreferedCategories;
  else
    result.PreferedCategories = [];
  
  if(message.length) return callback(message, result);
  else return callback(null, result);
}; 

exports.validatePassword = function(user , callback){
    if(user.Password){
      if(user.Password.length>=6){
        callback(null, crypto.createHash('md5').update(user.Password).digest("hex"));
      }else{
        callback("Password must be at least 6 letters", null);
      }
    }else{
       callback("No Password Provided", null);
    }
}; 

exports.validateBrandedApp = function(req, callback){
  var message =[]
  result = {}        
  //if(req._id)
  //  result._id = db.ObjectId(req._id);
  if(!req.files.Logo)
    message.Add("Logo is required");
  if(!req.files.Icon)
    message.Add("Icon is required");
  if(!req.Publisher)
    message.Add("Publisher is required");
  else
    result.Publisher = req.Publisher;
    
  if(!req.Authentication)
    message.Add("Authentication is required");
  else
    result.Authentication = req.Authentication;
    
  if(!req.files.ApplicationType)
     message.Add("Application Type is required");
  else
    result.ApplicationType = req.ApplicationType; 
  if(message.length) return callback(message, result);
  else return callback(null, result);
   
  
}
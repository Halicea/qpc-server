var _ = require('underscore'); 
var conf = require('../config');
var db = require("mongojs").connect(conf.dbName, conf.dbCollections)
var async = require("async");
var util = require('util');
var winston = require('winston');

var sendresponse = function(req, res, data){
	try{
	  if(req.query["callback"]){
  		res.header('content-type','text/javascript');
  		res.send(req.query["callback"]+'('+JSON.stringify(data)+')');
  	}else{
  		res.json(data);
  	}
	}catch(ex){
	   winston.log("error", ex.message);
	}
};             
exports.sendresponse = sendresponse;
exports.dbResponse = function(req, res, dataFunction){
  async.waterfall([
    function(callback){
      try{
        dataFunction(callback);
      }catch(ex){
        callback(ex, null);
      }
    }], function(err, results){
    if(err){
       sendresponse(req, res, {status:"error", Error:err.message});
    }else{
      if(results==null){
        sendresponse(req, res, {status:"ok", data:true});
      }
      else{
        sendresponse(req, res, {status:"ok", data:results});
      }
    }
  });
}; 

exports.sendNotAuthorized = function(req, res){
  res.send(401);
};

exports.setupAuth =function(everyauth){
  var fb = conf.providers.facebook;
  everyauth
    .facebook 
      .scope('email, user_status')
      .appId(fb.appId)
      .appSecret(fb.appSecret)
      .findOrCreateUser( function (session, accessToken, accessTokenExtra, fbUserMetadata) {           
        session.user = fbUserMetadata;
        return fbUserMetadata;
      })
     .redirectPath('/'); 
};

exports.redirectToLogin = function(req, res){
  res.redirect('/login');
};

exports.generateCrudRestUrls = function(app, base_match, obj){
  app.get(base_match, obj.index);
  app.post(base_match, obj.save);
  app.get(base_match+"/new", obj.view);
  app.get(base_match+"/:id", obj.view);
  app.get(base_match+"/:id/delete", obj.delete);
}

exports.loginRequired = function(onRedirectCallback){
  return function (req, res, next){
    if(req.session.user){
      next();
    }else{
      onRedirectCallback(req, res);
    }
  };
};
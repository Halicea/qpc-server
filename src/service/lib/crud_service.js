var utils = require('./utils.js') 
  , config = require('../config.js')
  , mongojs = require("mongojs")
	, db = mongojs.connect(config.dbName);

var defaultResponse = function(res){
  return function(err, data){
    if(!err){
      res.json({status:"ok", data:data});
    }else{ 
      console.log(err);
      res.json({status:"error", messages:[err]});
    }
  };
};      

//-----------
var CrudDb = function(coll){
  this.collection = coll;
  this.db = db;
};

CrudDb.prototype={ 
  save: function(obj, callback){
    if(typeof obj._id === "string"){
      if(obj._id)
        obj._id = db.ObjectId(obj._id);
      else{
        delete obj['_id'];
      }
    }
    db.collection(this.collection).save(obj, callback);
  },
  find: function(q, callback){ 
    if(!callback){
      callback = q;
      q ={};
    }
    db.collection(this.collection).find(q, callback);
  },
  delete: function(obj, callback){
    db.collection(this.collection).remove({_id:obj._id}, callback);
  },
  
  findOne: function(q, callback){
    db.collection(this.collection).findOne(q, callback);
  }
};

//-----------
var CrudSvc = function(dbSvc){
  this.dbSvc = dbSvc;
}

CrudSvc.prototype={
  defaultResponse : defaultResponse,
  view : function(req, res){
      this.dbSvc.findOne({_id:db.ObjectId(req.params.id)}, defaultResponse(res));
  },
  index:function(req, res){
     this.dbSvc.find(defaultResponse(res));
  },
  save:function(req, res){
    this.dbSvc.save(req.body, defaultResponse(res));
  },
  delete:function(req, res){
    this.dbSvc.delete({_id:db.ObjectId(req.params.id)}, defaultResponse(res));
  }
}          

exports.CrudService = CrudSvc; 
exports.CrudDb = CrudDb;
#!/usr/bin/env python
# encoding: utf-8
"""
prinstand_integration_tests.py
Created by Kosta Mihajlov on 2012-07-19.
Copyright (c) 2012 Halicea.Co. All rights reserved.
"""
import random
from hal_rest import *
token= ''  
import sys  
if len(sys.argv)>1:
  address = sys.argv[1] 
else:
  address = "http://qpc.halicea.com"    
  
stdh={"Content-Type":"application/json"}

to_test=[ 
  ("Index By Location", lambda: execute_request(None, "/svc/places/index", stdh, "POST", """
  {
    "Categories": [1, 2, 4],
    "Limit": 100,
    "MyLocation": [42.0000, 21.4333],
    "Radius": 2.5
  }
  """, None)),
  ("Search Places", lambda: execute_request(None, "/svc/places/search", stdh, "POST", """
  {  
    "SearchString":"Arabeska",
    "Categories": [1, 2, 4],
    "Limit": 100,
    "MyLocation": [42.0000, 21.4333],
    "Radius": 2.5
  }
  """, None)),  
  ("Get Place By Id", lambda: execute_request(None, "/svc/places/506434dd2d1df16d3400000c")),    
  ("Save Place",  lambda:execute_request(None, "/svc/places", stdh, "POST", """
  {
  "Name":"Arabeska",
  "Description":"Some test description",
  "CategoryId": 1,
  "CoverImageUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg",
  "Images":[],
  "Menu": {},    
  "Location":[42.0000, 21.4333] ,
  "SpecialOffers":[
     {
        "OfferId":1,
        "Name" : "Fish plate",
        "Description":"Tasty",
        "ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A",
        "Price":350
      }  
    ]
  }
  """), None)
] 


#seed_data()  
    
run_tests("Halicea QPC Integration Tests", address, to_test])

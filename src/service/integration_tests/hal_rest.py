#!/usr/bin/env python
# encoding: utf-8
"""
prinstand_integration_tests.py
Created by Kosta Mihajlov on 2012-07-19.
Copyright (c) 2012 Halicea.Co. All rights reserved.
"""
import requests    
import json 
import datetime  
import random
import traceback                                                                                                   
verbose = True
def run_tests(title, test_address, tests_dict):
  global address
  address = test_address  
  print "="*len(title)
  print title
  print "="*len(title)
  print
  print "  Generated On: :: ", datetime.date.today() 
  print
  for k in tests_dict:
    print k[0]
    print "="*len(k[0])
    print 
    k[1]()
    print
class ErrorResponse(object):
  def __init__(self, ex):
    self.status_code = ex.message
    self.text = traceback.format_exc()
    self.headers= ""

def set_address(test_address):
  global address
  address = test_address
      
def execute_request(token, selector, headers={}, method="GET", data="", file=None):
  req = method=="POST" and requests.post or requests.get
  res = None
  try:
    res = req(address+selector,data=data, cookies={"mwr.sid":token}, headers=headers, files =  file and {'file':file} or None) 
  except Exception, ex:
    res = ErrorResponse(ex)
    
  if verbose:
    print
    print "Request"
    print "+++++++"
    print
    print "  Selector:", selector
    print
    print "  Method:", method
    print
    print "  Data: ::",   
    if data:
      print
      print ''.join(['     '+x+'\n' for x in data.split('\n')] )    
    print
    print "  Headers: ::"
    print
    print "    "+str(headers)
    print
    print "Response:"
    print "+++++++++"   
    print
    print "  Status:", res.status_code    
    print
    print "  Headers: ::"        
    print 
    print '    '+str(res.headers)
    print
    print "  Response Content:  ::" 
    print 
    print ''.join(['     '+x+'\n' for x in res.text.split('\n')] )
  return res.text, res.status_code   







var middleware = require('./lib/middleware'),
    utils = require('./lib/utils'),  
    async = require("async"),
    controllers = {
      places: require("./controllers/svc/places.js"),     
      clients_log: require("./controllers/svc/clients_log.js")
    };                        

exports.setupUrls = function(app) {
  app.get("/svc/places/:id", controllers.places.view);
  app.post("/svc/places/search", controllers.places.search);
  app.post("/svc/places/index", controllers.places.index);
  app.post("/svc/places", controllers.places.save);    
  app.delete("/svc/places/:id", controllers.places.delete);
  app.post("/svc/places/vote/:id/:grade", utils.loginRequired, controllers.places.vote);  
  app.post("/svc/loger/error", controllers.clients_log.log_error);
  app.get("/svc/loger", controllers.clients_log.index);
};

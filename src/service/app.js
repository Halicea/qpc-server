var express = require('express') 
  , everyauth = require('everyauth')
  , socketIO = require('socket.io')
  , redis = require('redis')  
  , utils = require('./lib/utils')
  , urls = require('./urls')
  , conf = require('./config')
  , sessionStore = require('connect-redis')(express)
  , util = require('util')
  , winston = require('winston');

var app = module.exports = express.createServer();
winston.add(winston.transports.File, { filename: '/tmp/qpc_server.log' , handleExceptions: true});

everyauth.debug = conf.debug;
utils.setupAuth(everyauth);     
app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.set('view options', {layout:false});
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({ secret: "qpc secret", store: new sessionStore, key:"qpc.sid"}));
  app.use(everyauth.middleware());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
  //put everyauth in place
  everyauth.helpExpress(app);
  //setup url mappings to the controllers
  urls.setupUrls(app);
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});
app.configure('production', function(){
  //app.use(express.errorHandler());                                         
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
}); 

process.on('uncaughtException', function(ex) {
   winston.log("error",ex.message);
});
//End Configuration
                                                        
 
app.listen(process.env.VMC_APP_PORT || 1337, function(){
  winston.log("info", "QPC server listening on port "+app.address().port.toString()+" in "+app.settings.env.toString()+" mode");
});


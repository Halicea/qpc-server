=============================
Halicea QPC Integration Tests
=============================

  Generated On: ::  2012-09-27

Index By Location
=================


Request
+++++++

  Selector: /svc/places/index

  Method: POST

  Data: ::
     
       {
         "Categories": [1, 2, 4],
         "Limit": 100,
         "MyLocation": [42.0000, 21.4333],
         "Radius": 2.5
       }
       


  Headers: ::

    {'Content-Type': 'application/json'}

Response:
+++++++++

  Status: 200

  Headers: ::

    {'set-cookie': 'qpc.sid=EZ9dIujHCFrYiBZgzpvTQjRI.0y7SUbrnZ93iiUY97pR4DJsAAXHUkHmShWaCStEV%2Fts; path=/; expires=Thu, 27 Sep 2012 15:39:02 GMT; httpOnly', 'content-length': '2304', 'content-type': 'application/json; charset=utf-8', 'connection': 'keep-alive', 'x-powered-by': 'Express'}

  Response Content:  ::

     {"status":"ok","data":[{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"50643a97155faf353c000002"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"506434d02d1df16d3400000a"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"506434dd2d1df16d3400000c"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"506435402d1df16d3400000e"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"50643897155faf353c000001"}]}


Search Places
=============


Request
+++++++

  Selector: /svc/places/search

  Method: POST

  Data: ::
     
       {  
         "SearchString":"Arabeska",
         "Categories": [1, 2, 4],
         "Limit": 100,
         "MyLocation": [42.0000, 21.4333],
         "Radius": 2.5
       }
       


  Headers: ::

    {'Content-Type': 'application/json'}

Response:
+++++++++

  Status: 200

  Headers: ::

    {'set-cookie': 'qpc.sid=J9TWr2CJgQANizWOpVB0m3ne.XDjjj5ZotoKeYZk0tmpj2ewvBZ2Q6F1tUExAnYE6nF4; path=/; expires=Thu, 27 Sep 2012 15:39:02 GMT; httpOnly', 'content-length': '2304', 'content-type': 'application/json; charset=utf-8', 'connection': 'keep-alive', 'x-powered-by': 'Express'}

  Response Content:  ::

     {"status":"ok","data":[{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"50643a97155faf353c000002"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"506434d02d1df16d3400000a"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"506434dd2d1df16d3400000c"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"506435402d1df16d3400000e"},{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"50643897155faf353c000001"}]}


Get Place By Id
===============


Request
+++++++

  Selector: /svc/places/506434dd2d1df16d3400000c

  Method: GET

  Data: ::
  Headers: ::

    {}

Response:
+++++++++

  Status: 200

  Headers: ::

    {'set-cookie': 'qpc.sid=2E5QOqCJSEXgQTgBRLpPa9kg.GvPM5zhBl1I28kT0g99y4EkskZJxrB0nAl4jnaB0tWE; path=/; expires=Thu, 27 Sep 2012 15:39:02 GMT; httpOnly', 'content-length': '478', 'content-type': 'application/json; charset=utf-8', 'connection': 'keep-alive', 'x-powered-by': 'Express'}

  Response Content:  ::

     {"status":"ok","data":{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"506434dd2d1df16d3400000c"}}


Save Place
==========


Request
+++++++

  Selector: /svc/places

  Method: POST

  Data: ::
     
       {
       "Name":"Arabeska",
       "Description":"Some test description",
       "CategoryId": 1,
       "CoverImageUrl": "http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg",
       "Images":[],
       "Menu": {},    
       "Location":[42.0000, 21.4333] ,
       "SpecialOffers":[
          {
             "OfferId":1,
             "Name" : "Fish plate",
             "Description":"Tasty",
             "ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A",
             "Price":350
           }  
         ]
       }
       


  Headers: ::

    {'Content-Type': 'application/json'}

Response:
+++++++++

  Status: 200

  Headers: ::

    {'set-cookie': 'qpc.sid=LlyHdciodF8JHf8Oh5i1ueKy.zO%2BSHksQyEWUly39WWsLBFHpIPIwW1ZZD7h0p80E1o4; path=/; expires=Thu, 27 Sep 2012 15:39:02 GMT; httpOnly', 'content-length': '478', 'content-type': 'application/json; charset=utf-8', 'connection': 'keep-alive', 'x-powered-by': 'Express'}

  Response Content:  ::

     {"status":"ok","data":{"Name":"Arabeska","Description":"Some test description","CategoryId":1,"CoverImageUrl":"http://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Restaurant.jpg/250px-Restaurant.jpg","Images":[],"Menu":{},"Location":[42,21.4333],"SpecialOffers":[{"OfferId":1,"Name":"Fish plate","Description":"Tasty","ImageUrl":"http://t2.gstatic.com/images?q=tbn:ANd9GcRPkcIoTT-X0MrdtCuzJKP3BA5kSdMyv_oX7x-ZR8HfMWd7-8B40A","Price":350}],"_id":"50643ad6155faf353c000003"}}



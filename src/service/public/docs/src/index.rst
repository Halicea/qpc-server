.. Mesta.mk documentation master file, created by
   sphinx-quickstart on Wed Sep 26 05:00:10 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mesta.mk's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2
   
   specification


Indices and tables
==================

* :ref:`search`


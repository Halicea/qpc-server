var cache={
	places:[],
	currentLocation:[42.0000, 21.4333],
	currentPlace:null,
	radius : 20
}
function getPlaces(callback){
	$.post("/svc/places/index",{
	  MyLocation: cache.currentLocation,
	  Radius: cache.radius
	}).success(function (data){
		cache.places = data.data;
		callback(data);
	}).error(function(err){
	  alert(err.message);
	});
}
function placeDetails (index , callback){
  var place = cache.places[index];
	$.getJson("/svc/places/"+place._id.toString()).success(function(data){		
    cache.places[index] = data;
		callback(category, data);
	}).error(function(err){
	  alert(err);
	});
}

function searchPlaces (criteria, callback){
	$.post("/svc/places/search", criteria).sucess(function(data){
		callback(data);
	}).error(function(err){
	  alert(err); 
	});
} 

function setLocation(){
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      function(loc){
        cache.currentLocation = loc;
      }, 
      function(error){
        alert("Feature is Not Supported!");
      });
  } else {
    alert("Feature is Not Supported!");
  }
}

function getLocationString(loc){
  return loc[0].toString()+","+loc[1].toString();
} 

function getCategories(){
  return [
    1,2,4
  ];
}
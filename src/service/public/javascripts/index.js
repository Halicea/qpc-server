var selectedPlace;
var map;
setLocation();  

function mapDetailHandler(){
  $('[for="details"]').click(function(){
    var index= parseInt($(this).attr("index")); 
    var pl = cache.places[index];
    cache.currentPlace = pl;
    $('#placeDetails').html($("#placeDetailsTemplate" ).render(pl));     
    $('#placeGallery').html($("#placeGalleryTemplate" ).render(pl.Images)); 
    mapGallery();
    $("#placeTitle").html(pl.Name);
  });
}


function mapGallery(){
  $('#placeGallery a').photoSwipe({});
} 
function setPlaces(){
  getPlaces(function(data){
	  if(data.status=="ok"){
	    $('#places').html($("#placesTemplate" ).render(data.data));     
	    $('#places').listview("refresh"); 
	    mapDetailHandler(); 
	    //$('#places').collapsibleset( "refresh" );
	    for(var i=0; i<data.data.length;i++){  
	      var item =data.data[i]; 
	      var loc  = getLocationString(item.Location);
  	    map.addMarker({position:loc}).click(function(){
  	        map.openInfoWindow({'content':item.Description}, this);
  	    });
	    }
	  }else{
	    alert("error");
	  }
  });
}

$(document).bind('pageinit', function(){ 
  $('#map_canvas').gmap({"callback":function(){
      var self = this; // we can't use "this" inside the click function (that refers to the marker object in this example)
    	map = this;
    	self.getCurrentPosition(function(position, status) {
    		
    		if ( status === 'OK' ) {
       		var clientPosition = new google.maps.LatLng(cache.currentLocation[0], cache.currentLocation[1]);
    	   	cache.currentLocation =[position.coords.latitude, position.coords.longitude];
    			self.addMarker({'position': clientPosition, 'bounds': true}); 
    			setPlaces(); 
    			$('#map_canvas').gmap('option', 'zoom',12);  
    		}
    	});
  }});
  
	$('#btnSearchPlaces').click(function(){
		setLocation();
		searchPlaces({
		  SearchString:$("#searchBox").val(),
		  Categories: getCategories(),
		  Limit: 100,
		  MyLocation: cache.currentLocation,
		  Radius: 2.5
		}, function(data){	
			$('#publications').html($( "#publicationsTemplate" ).render(data.data));
		});
	}); 
}); 
